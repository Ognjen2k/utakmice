import React, { useCallback, useEffect, useState } from 'react';
import AutobuskaAxios from '../../apis/AutobuskaAxios';
import { useNavigate, useParams } from 'react-router-dom';
import { Row, Col, Button, Table, Form, NavItem } from 'react-bootstrap'


const Utakmice = (props) => {

    const routeParams = useParams();
    let navigate = useNavigate();

    const prazna_pretraga = {
        ReprezentacijaA: '',
        ReprezentacijaB: ''
    }

    const [search, setSearch] = useState(prazna_pretraga)
    const [showSearch, setShowSearch] = useState(false)
    const [totalPages, setTotalPages] = useState(0)
    const [pageNo, setPageNo] = useState(0)

    const [utakmice, setUtakmice] = useState([])
    const [reprezentacije, setReprezentacije] = useState([])

    const getUtakmice = (newPageNo) => {
        console.log(totalPages);
        const params = {
            ReprezentacijaA: search.ReprezentacijaA.naziv,
            ReprezentacijaB: search.ReprezentacijaB.naziv,
            pageNo: newPageNo
        }
        console.log(params);
        AutobuskaAxios.get('/utakmice', { params })
            .then(res => {
                setPageNo(newPageNo)
                console.log(parseFloat(res.headers.get('total-pages')));
                setTotalPages(res.headers.get('total-pages'))
                setUtakmice(res.data)

            })
            .catch(err => {
                alert('Doslo je do greske po preuzimanju utakmica')
            });
    }

    const getReprezentacije = useCallback(() => {
        AutobuskaAxios.get('/reprezentacije')
            .then(res => {
                setReprezentacije(res.data)
            })
            .catch(err => {
                alert('Doslo je do greke po preuzimanju reprezentacija')
            });
    }, [])

    useEffect(() => {
        getUtakmice(0)
        getReprezentacije()
        console.log(utakmice)
    }, [])

    const onReprezentacijaChange = (e, index) => {
        let reprezentacijaId = e.target.value;
        let reprezentacija = reprezentacije.find((reprezentacija) => reprezentacija.id == reprezentacijaId)
        if (index == 'a') {
            setSearch({
                ...search,
                ReprezentacijaA: reprezentacija
            })
        } else {
            setSearch({
                ...search,
                ReprezentacijaB: reprezentacija
            })
        }

    }
    const goToAdd = () => {
        navigate('/utakmice/dodaj');
    }

    const dodajGol=(utakmica,index)=>{
        let params={
            ...utakmica,
            reprezentacijaAId:utakmica.reprezentacijaA.id,
            ReprezentacijaBId:utakmica.reprezentacijaB.id
        }
        let reprezentacijaId=0;
        if(index==1){
            reprezentacijaId=params.reprezentacijaA.id
            params={
                ...params,
                goloviA:params.goloviA+1
            }
        }else{
            reprezentacijaId=params.reprezentacijaB.id
            params={
                ...params,
                goloviB:params.goloviB+1
            }
        }
        AutobuskaAxios.put(`/utakmice/${utakmica.id}`, params)
        .then(res=>{
            navigate('/strelci/'+reprezentacijaId)
            getUtakmice(pageNo)
            alert('A goal was added!')
         })
         .catch(err=>{
            console.log(err)
            alert('error while adding goals:(')
         })
    }
    // Objasnjenje funkcije:
    // utakmica=iz onog utakmica map, treba nam njen id da bi mogli da znamo za koju utakmicu dodajemo golove
    // (U put kontroleru imamo id kao path variable, dakle moramo njega da iscupamo nekako)
    // reprezentacijaAId:Napravljen je kontroler koji dobavlja sve igrace na osnovu reprezentaciaId, dakle ono sto nam je u repou
    // metoda getIgracByReprezentacijaId(Long id=@path variable long id = reprezentacijaId)
    // index nam treba da mi metoda znala da li ce da doda golove timu a ili b za odredjenu utakmicu odatle i ovaj if else, ako se pogleda dole u dugmetu 
    // videcemo da je i index prosledjen uz utakmicu kao parametar metode,



    const dodajGolB = (utakmica) => {
        let params = {
            ...utakmice,
            goloviB: utakmica.goloviB + 1
        }
        AutobuskaAxios.put('/utakmice/' + utakmica.id, params)
            .then(res => {
                console.log(utakmica.goloviB)

                alert('A goal was added successfully!')
                getUtakmice(pageNo)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured  while adding goals please try again!')
            });

    }

    const deleteUtakmica = (utakmicaId) => {
        AutobuskaAxios.delete('/utakmice/' + utakmicaId)
            .then(res => {
                setUtakmice((utakmice) => utakmice.filter(utakmica => utakmicaId != utakmicaId))
            })
            .catch(error => {
                console.log(error)
                alert('Error occured please try again!')
            });
    }
    var renderUtakmice = () => {
        return utakmice.map((utakmica) => {
            return (
                <tr key={utakmica.id}>
                    <td>{utakmica.reprezentacijaA.naziv}</td>
                    <td>{utakmica.reprezentacijaB.naziv}</td>
                    <td>{utakmica.goloviA}</td>
                    <td>{utakmica.goloviB}</td>
                    <td>
                        <Button onClick={() => deleteUtakmica(utakmica.id)}>Izbrisi utakmicu</Button>
                    </td>
                    <td>
                        <Button onClick={() => dodajGol(utakmica,1)}>A+1</Button>
                    </td>
                    <td>
                        <Button onClick={() => dodajGol(utakmica,2)}>B+1</Button>
                    </td>
                </tr>
            )
        })
    }


    const renderReprezentacije = () => {
        return reprezentacije.map((reprezentacija) => <option key={reprezentacija.id} value={reprezentacija.id}>{reprezentacija.naziv}</option>)
    }

    const renderSearchForm = () => {
        return (
            <>
                <Form style={{ width: '99%' }}>
                    <Form.Group>
                        <Form.Label>Reprezentacija A</Form.Label>
                        <Form.Select name="reprezentacija" value={search.ReprezentacijaA.id} onChange={(e) => onReprezentacijaChange(e, 'a')}>
                            <option></option>
                            {renderReprezentacije()}
                        </Form.Select><br />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Reprezentacija A</Form.Label>
                        <Form.Select name="reprezentacija" value={search.ReprezentacijaB.id} onChange={(e) => onReprezentacijaChange(e, 'b')}>
                            <option></option>
                            {renderReprezentacije()}
                        </Form.Select><br />
                    </Form.Group>
                </Form>
                <Row><Col>
                    <Button className='mt-3' onClick={() => getUtakmice(0)}>Pretraga</Button>
                </Col></Row>
            </>
        )
    }
    return (
        <Col>
            <Row><h1>Utakmice</h1></Row>
            <div>

                <Row><Col>
                    <Button onClick={() => goToAdd()}>Add</Button>
                </Col></Row>
                <label>
                    <input type="checkbox" onChange={() => setShowSearch(!showSearch)} />
                    Prikazi pretragu
                </label>
            </div>
            <Row hidden={!showSearch}>{renderSearchForm()}</Row><br />

            <Row><Col>
                <Table style={{ marginTop: 5 }}>
                    <thead>
                        <tr>
                            <th>Utakmica A</th>
                            <th>Utakmica B</th>
                            <th>Golovi A</th>
                            <th>Golovi B</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderUtakmice()}
                    </tbody>
                </Table>
                <Button disabled={pageNo === 0}
                    onClick={() => getUtakmice(pageNo - 1)}
                    className="mr-3">Prev</Button>
                <Button disabled={pageNo == totalPages - 1} onClick={() => getUtakmice(pageNo + 1)}>Next</Button>
            </Col></Row>
        </Col>
    )

}
export default Utakmice