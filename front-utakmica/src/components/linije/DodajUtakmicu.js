import React, { useState, useEffect, useCallback } from 'react';
import { Row, Col, Form, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import AutobuskaAxios from '../../apis/AutobuskaAxios';


function DodajLiniju(props) {

    // const linija = {
    //     brojMesta: 0,
    //     cenaKarte: 0,
    //     vremePolaska: '',
    //     destinacija: '',
    //     prevoznik: null
    // }

    const utakmica ={
        reprezentacijaA:'',
        reprezentacijaB:''
    }

    // const [novaLinija, setNovaLinija] = useState(linija);
    // const [prevoznici, setPrevoznici] = useState([]);

    const [novaUtakmica,setNovaUtakmica]=useState(utakmica);
    const [reprezentacije,setReprezentacije]=useState([]);

    let navigate = useNavigate();

    const getReprezentacije = useCallback(() => {
        AutobuskaAxios.get('/reprezentacije')
            .then(res => {
                setReprezentacije(res.data)
            })
            .catch(err => {
                alert('Doslo je do greke po preuzimanju reprezentacije')
            });
    }, [])

    useEffect(() => {
        getReprezentacije()
    }, [])

    const valueInputChanged = (e) => {
        let input = e.target;

        let name = input.name;
        let value = input.value;

        setNovaUtakmica(novaUtakmica => {
            novaUtakmica[name] = value;
            return novaUtakmica
        })
    }

    const reprezentacijaASelectionChanged = (e,re) => {
        let reprezentacijaId = e.target.value;
        let reprezentacija = reprezentacije.find((reprezentacija) => reprezentacija.id == reprezentacijaId);

        setNovaUtakmica(novaUtakmica => {

            return { ...novaUtakmica, reprezentacijaA: reprezentacija }
        });
    }

    const reprezentacijaBSelectionChanged = (e,re) => {
        let reprezentacijaId = e.target.value;
        let reprezentacija = reprezentacije.find((reprezentacija) => reprezentacija.id == reprezentacijaId);

        setNovaUtakmica(novaUtakmica => {

            return { ...novaUtakmica, reprezentacijaB: reprezentacija }
        });
    }

    const create = () => {
        let params = {
          reprezentacijaA:novaUtakmica.reprezentacijaA,
          reprezentacijaB:novaUtakmica.reprezentacijaB,
        }
        AutobuskaAxios.post("/utakmice", params)
            .then((res) => {
                // handle success
                console.log(res);
                alert("A match was added successfully!");
                navigate("/utakmice");
            })
            .catch((error) => {
                // handle error
                console.log(error);
                alert("Error occured while creating a match please try again!");
            });
    }

    const renderReprezentacije = () => {
        return reprezentacije.map((reprezentacija) => <option key={reprezentacija.id} value={reprezentacija.id}>{reprezentacija.naziv}</option>)
    }



    return (
        <>
            <Row>
                <Col>
                </Col>
                <Col xs="12" sm="10" md="8">
                    <h1>Dodaj utkamicu</h1>
                    <Form>
                        <Form.Group>
                            <Form.Label>Reprezentacija A</Form.Label>
                            <Form.Select name="ReprezentacijaA" onChange={(e) => reprezentacijaASelectionChanged(e)}>
                                <option> </option>
                                {renderReprezentacije()}
                            </Form.Select><br />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Reprezentacija B</Form.Label>
                            <Form.Select name="ReprezentacijaB" onChange={(e) => reprezentacijaBSelectionChanged(e)}>
                                <option> </option>
                                {renderReprezentacije()}
                            </Form.Select><br />
                        </Form.Group>
                        <Button onClick={(e)=>{ e.preventDefault(); create(props,novaUtakmica); }}>Dodaj utakmicu</Button>
                    </Form>
                </Col>
            </Row>
        </>
    )
}
export default DodajLiniju

