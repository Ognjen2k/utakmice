import React, { useCallback, useEffect, useState } from 'react';
import AutobuskaAxios from '../../apis/AutobuskaAxios';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { Row, Col, Button, Table, Form, NavItem } from 'react-bootstrap'

const Strelci = () => {
    const [igraci, setIgraci] = useState([])
    const [izabranIgrac, setIzabranIgrac] = useState([])

    const { reprezentacijaId } = useParams()

    let navigate=useNavigate()

    const getIgracByReprezentacijaId = useCallback(() => {
        AutobuskaAxios.get('igraci/reprezentacije/' + reprezentacijaId)
            .then(res => {
                setIgraci(res.data)
                console.log(igraci)
                // alert('Igraci uspesno dobavljeni')
            })
            .catch(err => {
                console.log(err)
                console.log('Nisi uspeo slepce:(')
            })
    }, [])

    const renderIgrace = () => {
        return igraci.map((igrac) => <option key={igrac.id} value={igrac.id}>{igrac.prezime}</option>)
    }

    useEffect(() => {
        getIgracByReprezentacijaId()
        console.log(igraci)
    }, [])

    const valueInputChanged = (e) => {
        let input = e.target;

        let name = input.name;
        let value = input.value;

        setIzabranIgrac(value)
    }

    const dodajGol = () => {
        console.log(izabranIgrac)
        AutobuskaAxios.put('/igraci/' + izabranIgrac)
            .then(res => {
                alert('Postignut je gol ankaraMesi');
                navigate('/utakmice')
            })
            .catch(err => {
                alert('Nisi uspeo:(')
            })
    }

    // Objasnjenje:
    // Route params=uzima id iz one index komponente zato je jako bitno da se i tamo zove reprezentacijaId a ne samo id,
    // Kao sto sam napisao u utakmice komponenti, to nam je potrebno da bi pogodili tacan id reprezentacije jer nam to treba kada gadjamo kontroler
    // Izabran igrac je zapravo long a ne ceo objekat zato sto smo mu u onom renderu dodelili value=igrac.id, na osnovu toga on je pogodio dobar kontroler
    // igrac dodajGol tako nesto, u tom kontroleru se radi put metoda nad entitom igrac, za razliku od utakmice komponente ovde je sva logika uradjena na 
    // backendu tako da se vide eto oba nacina kako moze da se uradi


    return (
        <>
            <Form style={{ width: '99%' }}>
                <Form.Group>
                    <Form.Label>Igraci</Form.Label>
                    <Form.Select name='igrac' value={izabranIgrac} onChange={(e) => valueInputChanged(e)}>
                        <option></option>
                        {renderIgrace()}
                    </Form.Select><br />
                </Form.Group>
                <td>
                    <Button onClick={e => dodajGol()}>Dodaj gol</Button>
                </td>
            </Form>
        </>
    )

}
export default Strelci;