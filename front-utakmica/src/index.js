import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import NotFound from './components/NotFound';
import Home from './components/Home';
import { Navbar, Nav, Button, Container } from 'react-bootstrap';
import { logout } from './services/auth';
import Login from './components/authorization/Login'
import { Route, Link, BrowserRouter as Router, Routes, UseNavigate, useParams, Navigate } from 'react-router-dom';
import Utakmice from './components/linije/Utakmice'
import DodajUtakmicu from './components/linije/DodajUtakmicu'
import Strelci from './components/linije/Strelci'


const App = () => {
    const routeParams = useParams();

    const jwt = window.localStorage.getItem("jwt")

    if (jwt) {
        return (
            <>
                <Router>
                    <Navbar expand bg="dark" variant="dark">
                        <Navbar.Brand as={Link} to="/"> JWD</Navbar.Brand>
                        <Nav>
                            <Nav.Link as={Link} to="/utakmice">Utakmice</Nav.Link>
                            <Button onClick={logout}>Log out</Button>
                        </Nav>
                    </Navbar>
                    <Container style={{ paddingTop: "10px" }} >
                        <Routes>
                            <Route path='/' element={<Home />} />
                            <Route path='/utakmice' element={<Utakmice />}></Route>
                            <Route path='/utakmice/dodaj' element={<DodajUtakmicu />}></Route>
                            <Route path='/strelci/:reprezentacijaId' element={<Strelci/>}></Route>
                            <Route path="/login" element={<Login />} />
                            <Route path="*" element={<NotFound />} />
                        </Routes>
                    </Container>
                </Router>
            </>
        )
    }else{
        return(
        <>
                <Router>
                    <Navbar expand bg="dark" variant="dark">
                        <Navbar.Brand as={Link} to="/"> JWD</Navbar.Brand>
                        <Nav>
                            <Nav.Link as={Link} to="/utakmice">Utakmice</Nav.Link>
                            <Button onClick={logout}>Log out</Button>
                        </Nav>
                    </Navbar>
                    <Container style={{ paddingTop: "10px" }} >
                        <Routes>
                            <Route path='/' element={<Home />} />
                            <Route path='/utakmice' element={<Utakmice />}></Route>
                            {/* <Route path='/linije/dodaj' element={<DodajLiniju />}></Route>
                            <Route path='/linije/izmeni/:id' element={<IzmeniLiniju />}></Route> */}
                            <Route path="/login" element={<Login />} />
                            <Route path="*" element={<NotFound />} />
                        </Routes>
                    </Container>
                </Router>
            </>
        )
    }


}
const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <App />,
)