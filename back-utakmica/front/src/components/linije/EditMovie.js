import React, { useState } from 'react';
import CinemaAxios from './../../apis/CinemaAxios';
import {Col, Row, Form, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const EditMovie = (props) => {

    console.log("RERENDER")
    const [movie, setMovie] = useState({
        movieId: props.selectedMovie.id,
        movieName: props.selectedMovie.naziv,
        movieDuration: props.selectedMovie.trajanje,
        movieGenres: props.selectedMovie.zanrovi
    })
    const navigate = useNavigate()

    const onInputChange = event => {
        const { name, value } = event.target;

        setMovie((movie) => {
            var new_movie = {...movie}
            new_movie[name] = value
            return new_movie
        });
    }

    const edit = () => {
        const params = {
            'id': movie.movieId,
            'naziv': movie.movieName,
            'trajanje': movie.movieDuration,
            'zanrovi': movie.movieGenres
        };

        CinemaAxios.put('/filmovi/' + movie.movieId, params)
        .then(res => {
            // handle success
            console.log(res);
            alert('Movie was edited successfully!');
            navigate('/movies');
        })
        .catch(error => {
            // handle error
            console.log(error);
            alert('Error occured please try again!');
         });
    }

    return (
        <Col>
            <Row><h1>Edit movie</h1></Row>
            <Row>
            <Form>
                <Form.Group>
                <Form.Label htmlFor="name">Name</Form.Label>
                <Form.Control name="movieName" type="text" value={movie.movieName} onChange={(e) => onInputChange(e)}/><br/>
                </Form.Group>
                <Form.Group>
                <Form.Label htmlFor="duration">Duration</Form.Label>
                <Form.Control name="movieDuration" type="number" value={movie.movieDuration} onChange={(e) => onInputChange(e)}/>
                </Form.Group>
            </Form>
            </Row>
            <Button className="button button-navy" onClick={() => edit()}>Edit</Button>
        </Col>
    );
}

export default EditMovie