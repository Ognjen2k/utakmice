import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";


const LinijaRow = (props) => {

    var navigate = useNavigate()



    const goToEdit = (linija) => {
        props.editCallback(linija);
        navigate('/linije/edit');
    }

    return (
        <tr>
           <td>{props.linija.prevoznikNaziv}</td>
                <td>{props.linija.destinacija}</td>                       
                 <td>{props.linija.brojMesta}</td>
                <td>{props.linija.vremePolaska}</td>
                <td>{props.linija.cenaKarte}</td>
           <td><Button variant="warning" onClick={() => goToEdit(props.linija)}>Edit</Button></td>
           <td><Button variant="danger" onClick={() => props.deleteLinijaCallback(props.linija.id)}>Delete</Button></td>
        </tr>
     )
}

export default LinijaRow;