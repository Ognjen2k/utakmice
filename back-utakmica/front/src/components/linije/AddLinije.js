import React, { useState, useEffect, useCallback } from 'react';
import { Row, Col, Form, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import AutobuskaAxios from '../../apis/AutobuskaAxios';
import { withNavigation } from '../../routeconf';

function AddLinija(props) {

    const empty_linija = {
        vremePolaska: "",
        destinacija: "",
        brojMesta: 0,
        cenaKarte: 0.00,
        prevoznikId: null
    }
    const [linija, setLinija] = useState(empty_linija)
    const [prevoznici, setPrevoznici] = useState([])
    const [isValid, setIsValid] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        getPrevoznici();
    }, [])

    const getPrevoznici = useCallback(() => {
        AutobuskaAxios.get("/prevoznici")
        .then(res => {
            console.log(res.data)
            setPrevoznici(res.data)
        })
        .catch(error => {
            console.log(error)
            alert('Greska prilikom preuzimanja prevoznika')
        })
    }, [])


    const valueInputChanged = (e) => {
        let input = e.target;

        let name = input.name;
        let value = input.value;

        setLinija(linija => {
            linija[name] = value
            console.log(linija)
            return linija
        });
        isInputValid()
    }

    const prevoznikSelectionChanged = (e) => {
        let prevoznikId = e.target.value;
        //let prevoznik = prevoznici.find((prevoznik) => prevoznik.id == prevoznikId);

        setLinija(linija => {
            linija.prevoznikId = prevoznikId
            return linija
        });
        isInputValid()
    }

    const create = () => {
        let linijaDTO = {
            vremePolaska: linija.vremePolaska,
            prevoznikId: linija.prevoznikId,
            destinacija: linija.destinacija,
            brojMesta: linija.brojMesta,
            cenaKarte: linija.cenaKarte
        }

        AutobuskaAxios.post("/linije", linijaDTO)
        .then(() => {
            navigate('/linije');
        })
        .catch(error => {
            console.log(error)
            alert('Error while creating line')
        })
    }

    const field_empty = (key) => {
        const value = linija[key]
        return (value == null || value == "" || value == 0)
    }

    const isInputValid = () => {
        //proveravamo da li je bilo koje polje ostalo prazno
        for (const key in linija) {
            if (linija.hasOwnProperty(key) && field_empty(key)){
                setIsValid(false)
                return
            }
        }

        const timeRegex = new RegExp( "^([01][0-9]|2[0-3]):[0-5][0-9]$");
        if(!timeRegex.test(linija.vremePolaska)){
                setIsValid(false)
                return
            }
        setIsValid(true)

    }

    const renderPrevoznici = () => {
        return prevoznici.map((prevoznik)=><option key={prevoznik.id} value={prevoznik.id}>{prevoznik.naziv}</option>)
    }

    return (
        <>
            <Row>
                <Col></Col>
                <Col xs="12" sm="10" md="8">
                    <h1>Add Linija</h1>
                    <Form>
                        <Form.Group>
                            <Form.Label>Vreme polaska</Form.Label>
                            <Form.Control
                                id="vremePolaska" name="vremePolaska" onChange={(e) => valueInputChanged(e)} /> <br />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Prevoznici</Form.Label>
                            <Form.Select id="prevoznik" name="prevoznik" onChange={(e) => prevoznikSelectionChanged(e)}> <br />
                            <option></option>
                            {renderPrevoznici()}
                            </Form.Select><br />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control id="destinacija" name="destinacija" onChange={(e) => valueInputChanged(e)} /> <br />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Broj mesta</Form.Label>
                            <Form.Control required type="number" step="1" id="brojMesta" name="brojMesta" onChange={(e) => valueInputChanged(e)} /> <br />
                        </Form.Group>
                            <Form.Label>Cena karte</Form.Label>
                            <Form.Control required type="number" step=".01" id="cenaKarte" name="cenaKarte" onChange={(e) => valueInputChanged(e)} /> <br />
                        <Form.Group>
                        </Form.Group>
                        <Button disabled={!isValid} onClick={(event) => { event.preventDefault(); create(props, linija); }}>Add</Button>
                    </Form>
                </Col>
                <Col></Col>
            </Row>
        </>
    )
}





export default withNavigation(AddLinija);