import React, {useState, useCallback, useEffect } from 'react';
import AutobuskaAxios from '../../apis/AutobuskaAxios';
import {Col, Row, Form, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

const EditLinija = (props) => {

    console.log("Ponovno iscrtavanje")
    const [linija, setLinija] = useState({
        linijaId: props.selectedLinija.id,
        linijaPrevoznikId: props.selectedLinija.prevoznikId,
        linijaDestinacija: props.selectedLinija.destinacija,
        linijaBrojMesta: props.selectedLinija.brojMesta,
        linijaVremePolaska: props.selectedLinija.vremePolaska,
        linijaCenaKarte: props.selectedLinija.cenaKarte
    })
    const [prevoznici, setPrevoznici] = useState([])
    const [isValid, setIsValid] = useState(false)
    const navigate = useNavigate()

//     const getLinijaById = useCallback((linijaId) => {
//         AutobuskaAxios.get('/linije/'+linijaId)
//         .then(res => {
// setUpdateLinija({linijaId: res.data.id,
//                 linijaPrevoznikId: res.data.prevoznikId,
//                 linijaDestinacija: res.data.destinacija,
//                 linijaBrojMesta: res.data.brojMesta,
//                 linijaVremePolaska: res.data.vremePolaska,
//                 linijaCenaKarte: res.data.cenaKarte        })
//         })
//         .catch(err => {
//             console.log(err)
//         })
//     }, [])

    // useEffect(() => {
    //     getLinijaById(linijaId)
    // }, [])
    useEffect(() => {
        getPrevoznici();
    }, [])

    const onInputChange = (event) => {
        const { name, value } = event.target;

        setLinija((linija) => {
            var new_linija = {...linija}
            new_linija[name] = value
            return new_linija
        });
    }

    const prevoznikSelectionChanged = (e) => {
        let prevoznikId = e.target.value;
        //let prevoznik = prevoznici.find((prevoznik) => prevoznik.id == prevoznikId);

        setLinija(linija => {
            linija.prevoznikId = prevoznikId
            return linija
        });
        isInputValid()
    }

    const isInputValid = () => {
        //proveravamo da li je bilo koje polje ostalo prazno
        for (const key in linija) {
            if (linija.hasOwnProperty(key) && field_empty(key)){
                setIsValid(false)
                return
            }
        }

        const timeRegex = new RegExp( "^([01][0-9]|2[0-3]):[0-5][0-9]$");
        if(!timeRegex.test(linija.vremePolaska)){
                setIsValid(false)
                return
            }
        setIsValid(true)

    }

    const field_empty = (key) => {
        const value = linija[key]
        return (value == null || value == "" || value == 0)
    }

    const edit = () => {
        const params = {
            'id': linija.linijaId,
            'prevoznikId': linija.prevoznikId,
            'destinacija': linija.destinacija,
            'brojMesta': linija.brojMesta,
            'vremePolaska': linija.vremePolaska,
            'cenaKarte': linija.cenaKarte
        }

        AutobuskaAxios.put('/linije/'+linija.linijaId, params)
        .then(res => {
            console.log(res)
            alert('Linija je uspesno izmenjena.')
            navigate('/linije')
        })
        .catch(err => {
            console.log(err)
            alert('Neuspesna izmena, pokusajte ponovo!')
        })
    }

    const getPrevoznici = useCallback(() => {
        AutobuskaAxios.get("/prevoznici")
        .then(res => {
            console.log(res.data)
            setPrevoznici(res.data)
        })
        .catch(error => {
            console.log(error)
            alert('Greska prilikom preuzimanja prevoznika')
        })
    }, [])

    const renderPrevoznici = () => {
        return prevoznici.map((prevoznik)=><option key={prevoznik.id} value={prevoznik.id}>{prevoznik.naziv}</option>)
    }



    return (
        <>
        <Row>
            <Col></Col>
            <Col xs="12" sm="10" md="8">
                <h1>Edit Linija</h1>
                <Form>
                    <Form.Group>
                        <Form.Label>Vreme polaska</Form.Label>
                        <Form.Control
                            id="vremePolaska" name="vremePolaska" value={linija.vremePolaska} onChange={(e) => onInputChange(e)} /> <br />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Prevoznici</Form.Label>
                        <Form.Select id="prevoznik" name="prevoznik" value={linija.prevoznikId} onChange={(e) => prevoznikSelectionChanged(e)}> <br />
                        <option></option>
                        {renderPrevoznici()}
                        </Form.Select><br />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Destinacija</Form.Label>
                        <Form.Control id="destinacija" name="destinacija" onChange={(e) => onInputChange(e)} /> <br />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Broj mesta</Form.Label>
                        <Form.Control required type="number" step="1" id="brojMesta" name="brojMesta" onChange={(e) => onInputChange(e)} /> <br />
                    </Form.Group>
                        <Form.Label>Cena karte</Form.Label>
                        <Form.Control required type="number" step=".01" id="cenaKarte" name="cenaKarte" onChange={(e) => onInputChange(e)} /> <br />
                    <Form.Group>
                    </Form.Group>
                    <Button onClick={() => edit()}>Edit</Button>
                </Form>
            </Col>
            <Col></Col>
        </Row>
    </>
    )
}

export default EditLinija;