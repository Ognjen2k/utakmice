import React, { useEffect, useState } from 'react';
import AutobuskaAxios from '../../apis/AutobuskaAxios';
import { useNavigate } from 'react-router-dom';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import LinijaRow from './LinijaRow';

const Linije = (props) => {

    const empty_search = {
        destinacija: "",
        cenaKarteDo: "",
        id: ""
    }
    const [search, setSearch] = useState(empty_search)
    const [totalPages, setTotalPages] = useState(0)
    const [linije, setLinije] = useState([])
    const [pageNo, setPageNo] = useState(0)
    const [prevoznici, setPrevoznici] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        getLinije(0)
        getPrevoznici()
    }, [])

    const getPrevoznici = () => {
        AutobuskaAxios.get("/prevoznici")
        .then((resp) => {
            setPrevoznici(resp.data)
        })
        .catch((err=>{console.log(err)}))
    }

    const getLinije = (newPageNo) => {
        const conf = {
            params: {
                destinacija: search.destinacija,
                cenaKarteDo: search.cenaKarteDo,
                prevoznikId: search.id,
                pageNo: newPageNo
            }
        }

        AutobuskaAxios.get("/linije", conf)
            .then(res => {
                console.log(res)
                setLinije(res.data)
                setPageNo(newPageNo)
                setTotalPages(res.headers['total-pages'])
            })
            .catch(error => {
                console.log(error)
                alert('Error while fetching lines')
            })
    }

    const goToAdd = () => {
        navigate('/linije/add')
    }

    const renderSearchForm = () => {
        return (
            <>
            <Form style={{width: '99%'}}>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Destinacija</Form.Label>
                            <Form.Control
                                name = "destinacija"
                                as = "input"
                                type = "text"
                                onChange={(e) => {onInputChange(e)}}>
                            </Form.Control>
                        </Form.Group>
                    </Col>

                </Row>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Prevoznik</Form.Label>
                            <Form.Select name="id" onChange={(e)=>onInputChange(e)}>
                            <option value=""></option>
                            {prevoznici.map((prevoznik)=>{
                                return(
                                    <option value={prevoznik.id}>{prevoznik.naziv}</option>
                                );
                            })}
                        </Form.Select>
                        </Form.Group>
                    </Col>

                </Row>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Max cena karte</Form.Label>
                            <Form.Control
                                name = "cenaKarteDo"
                                as = "input"
                                type = "number"
                                onChange={(e) => {onInputChange(e)}}>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                </Row>
                <Row><Col>
                <Button className="btn btn-danger" onClick={() => getLinije(0)}>Search</Button><br/>
            </Col></Row>
            </Form>
            
            
            </>
        )

        }

        const onInputChange = (event) => {
            const name = event.target.name;
            const value = event.target.value
    
            search[name] = value;
    
            setSearch(search)
        }

        const deleteLinija = (linijaId) => {
            AutobuskaAxios.delete('/linije/' + linijaId)
                .then(res => {
                    // handle success
                    console.log(res)
                    alert('Linija je uspesno obrisana!')
                    setLinije((linije)=>linije.filter(linija => linija.id != linijaId))
                })
                .catch(error => {
                    // handle error
                    console.log(error)
                    alert('Error occured please try again!')
                });
        }

        // const goToEdit = (linija) => {
        //     navigate('/linije/edit/'+ linija)
        // }

        const renderLinije = () => {
            return linije.map((linija) => {
                return <LinijaRow key={linija.id} linija={linija} editCallback={props.callback} deleteLinijaCallback={deleteLinija}></LinijaRow>
            })
        }


    return (
        <div>
            <h1>Linije</h1>
            {renderSearchForm()}
            <div>
                <Button onClick={() => goToAdd()}>Add</Button>
                <br />

                <Table id="movies-table">
                    <thead>
                        <tr>
                            <th>Naziv prevoznika</th>
                            <th>Destinacija</th>
                            <th>Broj mesta</th>
                            <th>Vreme polaska</th>
                            <th>Cena karte</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderLinije()}
                    </tbody>
                </Table>
                <Button disabled={pageNo===0} 
                onClick={()=>getLinije(pageNo-1)}
                className='mr-3'>Prev</Button>
                <Button disabled={pageNo==totalPages-1 || totalPages==0}
                onClick={()=>getLinije(pageNo+1)}>Next</Button>
            </div>
        </div>
    )
}

export default Linije;